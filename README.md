JavaMug
=======

Overview
--------

JavaMug aims to support extracting and storing java class files
from/to some archives.

Requirements
------------

* Runtime/Development Environment
    * Java SE 7
* Project Management
    * [Maven 3.x](http://maven.apache.org/)
* Dependencies for Compilation
    * [ASM 5.0.3](http://asm.objectweb.org/)
* Test Environment
    * [JUnit 4.11](http://www.junit.org/)

Authors
-------

 Author: Associate Professor, Haruaki Tamada (Ph.D) 
 Affiliation: Faculty of Computer Science and Engineering, Kyoto Sangyo University.
 E-mail: tamada_f@ke_cc.kyoto-su.ac.jp
 Web Page: http://bitbucket.org/htamada/javamug/

License
-------

Apache License Version 2.0

    Copyright 2013- Haruaki Tamada

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
